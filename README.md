# rollup-plugin-shebang

[![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo) [![Coverage Status](https://coveralls.io/repos/bitbucket/robmarr/rollup-plugin-shebang/badge.svg?branch=master)](https://coveralls.io/bitbucket/robmarr/rollup-plugin-shebang?branch=master) [![](https://img.shields.io/bitbucket/pipelines/robmarr/rollup-plugin-shebang.svg)](https://bitbucket.org/robmarr/rollup-plugin-shebang/addon/pipelines/home) [![](https://img.shields.io/npm/v/@robmarr/rollup-plugin-shebang/latest.svg)]() [![](https://img.shields.io/discord/536945285989924864.svg)](https://discord.gg/X2jCtr6)

A Rollup plugin to add a shebang to the top of a bundle.

## Usage

Install the plugin with NPM:

```sh
npm i -D @robmarr/rollup-plugin-shebang
```

Add it to your rollup configuration:

```js
import shebang from '@robmarr/rollup-plugin-shebang'

export default {
  plugins: [
    shebang()
  ]
}
```

by default `#!/usr/bin/env node` is added but an alternative shebang can be used by passing it as an argument.

```js
import shebang from '@robmarr/rollup-plugin-shebang'

export default {
  plugins: [
    shebang('#!/usr/local/bin/node')
  ]
}

```

## Thanks

[yingye](https://github.com/yingye) for [rollup-plugin-banner](https://github.com/yingye/rollup-plugin-banner/), it was the template for this plugin.
