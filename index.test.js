import test from 'ava'
import {rollup} from 'rollup'

import shebang from '.'

test('use plugin to prepend `#!/usr/bin/env node`', async t => {
  const bundle = await rollup({input: 'test/fixtures/prepend.js', plugins: [shebang()]})
  const {output} = await bundle.generate({format: 'cjs'})
  t.snapshot(output[0].code)
})

test('use plugin to prepend an alternative shebang', async t => {
  const bundle = await rollup({input: 'test/fixtures/prepend.js', plugins: [shebang('#!shebang')]})
  const {output} = await bundle.generate({format: 'cjs'})
  t.snapshot(output[0].code)
})
