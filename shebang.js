export default class ShebangPlugin {
  constructor(alt) {
    this.shebang = typeof alt === 'string' ? alt : '#!/usr/bin/env node'
  }

  prependShebang(code) {
    return this.shebang + '\n\n' + code
  }
}
