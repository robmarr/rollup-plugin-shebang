import ShebangPlugin from './shebang'

export default function shebang(alt) {
  const plugin = new ShebangPlugin(alt)
  return {
    name: 'shebang',
    renderChunk(code) {
      return plugin.prependShebang(code)
    }
  }
}
